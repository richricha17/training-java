package com.rm.bankbusiness;

public class Vehicle {
	String vehicleNumber;
	String vehicleType;
	double costPerKm;
	boolean isAvailable;

	Vehicle(String vehicleNumber, String vehicleType, double costPerKm, boolean isAvailable) {
		this.vehicleNumber = vehicleNumber;
		this.vehicleType = vehicleType;
		this.costPerKm = costPerKm;
		this.isAvailable = isAvailable;
	}

	@Override
	public String toString() {
		return "Vehicle [vehicleNumber=" + vehicleNumber + ", vehicleType=" + vehicleType + ", costPerKm=" + costPerKm
				+ ", isAvailable=" + isAvailable + "]";
	}

	public static void main(String[] args) {
		Vehicle v = new Vehicle("KA03EG4566", "petrol", 12.5, true);
		System.out.println(v);

}
}