package com.rm.bankbusiness;

public class Account {
	long accountNumber;
	String accountHolderName;
	String accountType;
	double balance;

	Account(long accountNumber, String accountHolderName, String accountType, double balance) {
		this.accountNumber = accountNumber;
		this.accountHolderName = accountHolderName;
		this.accountType = accountType;
		this.balance = balance;
	}
	

	@Override
	public String toString() {
		return "Account [accountNumber=" + accountNumber + ", accountHolderName=" + accountHolderName + ", accountType="
				+ accountType + ", balance=" + balance + "]";
	}


	public static void main(String[] args) {
		Account a = new Account(12345678L, "richa", "savings", 5000.0);
		System.out.println(a);
		Account a2 = new Account (3456789231L, "ashu","checking",5679);
		System.out.println(a2);
		a2.balance+=100;
		System.out.println(a2);
		
		

	}
}
