package com.rm.sampleconstructor;

public class Student {
String studentName ;
int age;
int rollNo;
int avgPercentage;
 


 Student(int rollNo, String studentName, int age) {
	
	this.rollNo = rollNo;
	this.studentName = studentName;
	this.age = age;
}



@Override
public String toString() {
	return "Student [studentName=" + studentName + ", age=" + age + ", rollNo=" + rollNo + ", avgPercentage="
			+ avgPercentage + "]";
}



public static void main(String[] args) {
	Student s1=new Student (1,"alpha",25);
	System.out.println(s1);
	s1.avgPercentage=67;

	System.out.println(s1);
}


}
