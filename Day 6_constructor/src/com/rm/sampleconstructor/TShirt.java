package com.rm.sampleconstructor;

public class TShirt {
	String color ;
	int size;
	String brand;
	TShirt(String color, int size, String brand) {
		this.color = color;
		this.size = size;
		this.brand = brand;
	}
	TShirt(String color) {
		this.color = color;
	}
	TShirt(String color, int size) {
		this.color = color;
		this.size = size;
	}
	
	public static void main(String[] args) {
		TShirt t1 = new TShirt( "blue",42);
		System.out.println(t1);
		
		TShirt t2 = new TShirt ("black");
		System.out.println(t2);
		
		TShirt t3 = new TShirt ("grey",42,"Reebock");
        System.out.println(t3);	

	}
	@Override
	public String toString() {
		return "TShirt [color=" + color + ", size=" + size + ", brand=" + brand + "]";
	}
	
	

}
