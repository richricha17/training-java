package com.rm.sampleconstructor;

public class Mobile {
	String color;
	int storageCapacity;

	public static void main(String[] args) {
		Mobile m1 = new Mobile();
		System.out.println(m1);

		Mobile m2 = new Mobile();
		m2.color = "black";
		System.out.println(m2);

	}

	@Override
	public String toString() {
		return "Mobile [color=" + color + ", storageCapacity=" + storageCapacity + "]";
	}
}
