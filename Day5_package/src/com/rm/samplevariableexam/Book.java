package com.rm.samplevariableexam;

public class Book {
 String bookId ;
 String bookName;
 double price ;
 String language;
 
 Book(String bookId,String bookName,double price,String language) {
	 this.bookId = bookId;
	 this.bookName =bookName;
	 this.language =language ;
	 this.price = price;
 }
 public static void main(String[] args) {
	Book b1 = new Book ( "t07", "BTD",500,"Eng");
	System.out.println(b1);
	Book b2 =new Book ("t09","RAC",499.50,"Eng");
	System.out.println(b2);
}
 }



