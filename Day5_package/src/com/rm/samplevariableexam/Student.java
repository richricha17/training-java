package com.rm.samplevariableexam;

public class Student {
//instance variable
	String name;
	int age;
	String collegeName ="xyzit";
 
	public static void main(String[] args) {
		Student s1 =new Student ();
		System.out.println(s1.name);//null
		System.out.println(s1.age);//0
		System.out.println(s1.collegeName);//xyzit
		s1.name= "alpha";
		s1.age =20;
		
		Student s2 =new Student ();
		s2.name ="Beta";
		s2.age = 21;
		s2.collegeName ="Oxford";
	}
			
}
