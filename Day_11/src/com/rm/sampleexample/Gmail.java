package com.rm.sampleexample;

public class Gmail {
	static void login(String emailAddress, String password) {
		System.out.println("Logged in using email address:" + emailAddress);
	}

	static void login(long phoneNumber, String password) {
		System.out.println("Logged in using phone number:" + phoneNumber);
	}

	static void login(long phoneNumber, int otp) {
		System.out.println("Logged in using phone number:" + phoneNumber);
	}

	public static void main(String[] args) {
		login("richricha@gmail.com", "richa@123");
		login(9786547688L, "richa@122");
		login(9876543210L, 123456);

	}
}
