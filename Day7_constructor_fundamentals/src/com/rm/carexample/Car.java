package com.rm.carexample;

public class Car {
	String brand ="Rolls Royce";
	String color;
	char variant;
	double mileage;
	Car(String color, char variant, double mileage) {
		this.color = color;
		this.variant = variant;
		this.mileage = mileage;
	}
	public static void main(String[] args) {
		Car c1 = new Car ("blue",'P',4.5);
		
		System.out.println(c1);
		Car c2 =new Car ("black");
		System.out.println(c2);
		
	}
	Car(String color) {
		this.color = color;
		this.variant ='p';
		this.mileage= 5.5;
		
	}
	
	@Override
	public String toString() {
		return "Car [brand=" + brand + ", color=" + color + ", variant=" + variant + ", mileage=" + mileage + "]";
	}


	

}
